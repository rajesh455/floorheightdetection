//
//  FloorInfoCell.swift
//  Barometer Demo
//
//  Created by Rajesh Kollipara on 10/02/19.
//  Copyright © 2019 Rajesh Kollipara. All rights reserved.
//

import UIKit

class FloorInfoCell: UITableViewCell {

    @IBOutlet weak var labelPressure: UILabel!
    @IBOutlet weak var labelFloorHeight: UILabel!
    @IBOutlet weak var labelFloorNumber: UILabel!
    @IBOutlet weak var labelTimeStamp: UILabel!
    @IBOutlet weak var labelComments: UILabel!
    @IBOutlet weak var labelCoordinates: UILabel!
    @IBOutlet weak var labelWifiInfo: UILabel!
    @IBOutlet weak var labelMagneticInfo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
