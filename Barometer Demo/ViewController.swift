//
//  ViewController.swift
//  Barometer Demo
//
//  Created by Rajesh Kollipara on 10/01/19.
//  Copyright © 2019 Rajesh Kollipara. All rights reserved.
//

import UIKit
import CoreMotion
import CoreData
import CoreLocation
import UserNotifications
import UserNotificationsUI
import CoreMotion
let appDelegate = UIApplication.shared.delegate as! AppDelegate


final class ViewController: UIViewController {
    @IBOutlet weak var buildingInfoTable: UITableView!
    @IBOutlet weak var textView: UITextView!
    let locationManager = CLLocationManager()
    let motionManager   = CMMotionManager()
    var currentLocation: CLLocationCoordinate2D?
    
    var tableData = [HeightInfo]() {
        
        didSet {
            
            buildingInfoTable.reloadData()
        }
    }
    @IBOutlet weak var labelPressure: UILabel!
    @IBOutlet weak var labelAltitude: UILabel!
    @IBOutlet weak var labelHeight: UILabel!
    @IBOutlet weak var labelCoordinates: UILabel!
    @IBOutlet weak var fieldFloorHeight: UITextField!
    @IBOutlet weak var labelMagnetometerData: UILabel!
    @IBOutlet weak var labelWifiInfo: UILabel!
    @IBOutlet weak var labelBeaconsInfo: UILabel!
    
    let context = appDelegate.persistentContainer.viewContext
    
    
    lazy var altimeter = CMAltimeter()
    var firstAltimeterData: CMAltitudeData?
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.appDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        textView.addDoneButtonOnKeyboard()
        startMagnetometer()
        fieldFloorHeight.addDoneButtonOnKeyboard()
        setCornerRadius()
        buildingInfoTable.tableFooterView = UIView()
        retrieveInfoFromCoreData()
        _ = NetworkManager.getNetworkInfos().map {
            
            let wifiInfo = "SSID: \($0.ssid)\n MAC ID: \($0.bssid)"
            labelWifiInfo.text = wifiInfo

        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func startMagnetometer() {
        
        if motionManager.isDeviceMotionAvailable {
            motionManager.deviceMotionUpdateInterval = 1
            motionManager.showsDeviceMovementDisplay = true

            
            motionManager.magnetometerUpdateInterval = 1
            motionManager.startDeviceMotionUpdates(using: .xArbitraryCorrectedZVertical, to: OperationQueue.main) { (deviceMotion, error) in
                
                if let motion = deviceMotion {
                    _ = motion.magneticField.accuracy
                    let x = motion.magneticField.field.x
                    let y = motion.magneticField.field.y
                    let z = motion.magneticField.field.z
                    let magneticValue = "x: \(x)\n y: \(y)\n z: \(z)"
                    self.labelMagnetometerData.text = magneticValue
                }
                else {
                    print("Device motion is nil.")
                }
            }
        }
        
    }
    
    @objc  func appDidBecomeActive() {
        setUpLocationManager()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    private func setCornerRadius() {
        
        textView.layer.cornerRadius = 1.0
        textView.layer.borderColor = UIColor.black.cgColor
        textView.layer.borderWidth = 1.0
    }
    private func startTracking() {
        
        
        guard isBarometerAvailable() else {
            return
        }
        
        startUpdates()
        
    }
    
    private func startUpdates() {
        altimeter.startRelativeAltitudeUpdates(to: OperationQueue.main) { [weak self] (data, error) in
            guard let weakSelf = self, let altitudeData = data else {
                return
            }
            
            
            weakSelf.updateUI(altitudeData: altitudeData)
            
        }
        
    }
    
    private func updateUI(altitudeData: CMAltitudeData) {
        
        if  firstAltimeterData == nil {
            firstAltimeterData = altitudeData
        }
        
        self.labelAltitude.text = "\(altitudeData.relativeAltitude)" + " m"
        self.labelPressure.text = "\(altitudeData.pressure)" + " pa"
        //P0 Sea-level pressure in hPa
        let P0 = 1013.25
        //P1 is atmospheric pressure in hPa
        let P1 = altitudeData.pressure.doubleValue * 10
        //t is temperature in C
        //For demonstration purpose I'm setting a temporary value.
        let t =  21.0
        
        //Ref For Formula: https://keisan.casio.com/exec/system/1224585971
        let height = ((pow(P0/P1,1/5.257)-1)*(t+273.15))/0.0065;
        self.labelHeight.text = "\(height)" + " m"
        
    }
    private func isBarometerAvailable() -> Bool {
        
        return CMAltimeter.isRelativeAltitudeAvailable()
    }
    
    private func stopTracking() {
        
        altimeter.stopRelativeAltitudeUpdates()
    }
    
    @IBAction func startMonitoring(_ sender: UIButton) {
        startTracking()
        
    }
    
    @IBAction func clearData(_ sender: UIButton) {
        
        self.clearTable()
    }
    
    @IBAction func actionStoreCurrentValue(_ sender: UIButton) {
        view.endEditing(true)
        guard let floorHeight = Double(fieldFloorHeight.text ?? "")  else {
            self.showAlertviewController(titleName: "", messageName: "Plese set height before storing Value.", cancelButtonTitle: "OK", otherButtonTitles: [], selectionHandler: {_ in })
            return
        }
        
        guard floorHeight > 0.0 else {
            self.showAlertviewController(titleName: "", messageName: "Plese set a minimum Floor Height of 1", cancelButtonTitle: "OK", otherButtonTitles: [], selectionHandler: {_ in })
            return
            
        }
        
        guard let firstHeight = self.firstAltimeterData?.relativeAltitude as? Double else {
            self.showAlertviewController(titleName: "", messageName: "Please Start Monitoring.", cancelButtonTitle: "ok", otherButtonTitles: [], selectionHandler: {_ in })
            return
        }
        let difference =   ((self.labelAltitude.text ?? "") as NSString).doubleValue - firstHeight
        
        // if difference > 0 {
        let floorNumber = difference / floorHeight
        
        let heightInfoModel = NSEntityDescription.insertNewObject(forEntityName: "HeightInfo", into: context) as! HeightInfo
        
        heightInfoModel.time = Date() as NSDate
        heightInfoModel.floor = Int32(floorNumber)
        heightInfoModel.pressue = ((self.labelPressure.text ?? "") as NSString).doubleValue
        heightInfoModel.height = Int32(floorHeight)
        heightInfoModel.comments = textView.text
        heightInfoModel.wifiInfo = labelWifiInfo.text
        heightInfoModel.magneticInfo = labelMagnetometerData.text
        if let coordinates = self.currentLocation {
            heightInfoModel.latitude = coordinates.latitude
            heightInfoModel.longitude = coordinates.longitude
        }
        do {
            try context.save()
            textView.text = ""
        } catch let error as NSError {
            print("Saving error: \(error.localizedDescription)")
        }
        retrieveInfoFromCoreData()
        //    }
        
        
    }
    
    
}

extension UIViewController {
    
    func showAlertviewController(titleName: String, messageName: String, cancelButtonTitle: String, otherButtonTitles: [String], selectionHandler: @escaping (Int) -> Void ) {
        
        let alertController = UIAlertController(title: NSLocalizedString(titleName, comment: ""), message: NSLocalizedString(messageName, comment: ""), preferredStyle: .alert)
        let alertActionOk = UIAlertAction(title: NSLocalizedString(cancelButtonTitle, comment: ""), style: .cancel, handler: { alert in
            
            selectionHandler(alertController.actions.index(of: alert)!)
        })
        otherButtonTitles.forEach {
            
            let alertAction = UIAlertAction(title: NSLocalizedString($0, comment: ""), style: .default, handler: { alert in
                selectionHandler(alertController.actions.index(of: alert)!)
            })
            alertController.addAction(alertAction)
            
        }
        
        alertController.addAction(alertActionOk)
        self.present(alertController, animated: true, completion: nil)
        
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let floorCell =  tableView.dequeueReusableCell(forIndexPath: indexPath) as FloorInfoCell
        let model = self.tableData[indexPath.row]
        floorCell.labelFloorHeight.text = "\(model.height)" + "  m"
        floorCell.labelPressure.text =  "\(model.pressue)" + "  pa"
        floorCell.labelTimeStamp.text = (model.time as Date?)?.string(with: "dd/MM/yyyy HH:mm:ss")
        floorCell.labelFloorNumber.text = "\(model.floor)"
        floorCell.labelComments.text = model.comments ?? ""
        floorCell.labelWifiInfo.text = model.wifiInfo
        floorCell.labelMagneticInfo.text = model.magneticInfo
        //        let lat = String(format:"%.5f", model.latitude)
        //        let long = String(format:"%.5f", model.longitude)
        floorCell.labelCoordinates.text = "( " + "\(model.latitude)" + " , " + "\(model.longitude)" + " )"
        return floorCell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


extension ViewController {
    
    func retrieveInfoFromCoreData() {
        let request: NSFetchRequest<HeightInfo> = HeightInfo.fetchRequest()
        let sort = NSSortDescriptor(key: #keyPath(HeightInfo.time), ascending: false)
        request.sortDescriptors = [sort]
        request.returnsObjectsAsFaults = false
        guard let floorInfoArray = try? context.fetch(request) else { return }
        self.tableData = floorInfoArray
    }
    
    
    
    func clearTable() {
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "HeightInfo")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
        self.tableData = [ HeightInfo]()
    }
    
}

extension Date {
    func string(with format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}



extension UITextView{
    
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}

extension UITextField{
    
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}

extension ViewController: CLLocationManagerDelegate {
    
    func setUpLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.requestWhenInUseAuthorization()
        guard CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
        
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let coordinates = manager.location?.coordinate else {
            return
        }
        self.currentLocation = coordinates
        let lat = String(format:"%.4f", coordinates.latitude)
        let long = String(format:"%.4f", coordinates.longitude)
        
        labelCoordinates.text = "(\(lat),\(long))"
    }
}
