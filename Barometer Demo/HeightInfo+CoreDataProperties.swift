//
//  HeightInfo+CoreDataProperties.swift
//  Barometer Demo
//
//  Created by Rajesh Kollipara on 31/03/19.
//  Copyright © 2019 Rajesh Kollipara. All rights reserved.
//
//

import Foundation
import CoreData


extension HeightInfo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<HeightInfo> {
        return NSFetchRequest<HeightInfo>(entityName: "HeightInfo")
    }

    @NSManaged public var comments: String?
    @NSManaged public var floor: Int32
    @NSManaged public var height: Int32
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var pressue: Double
    @NSManaged public var time: NSDate?
    @NSManaged public var wifiInfo: String?
    @NSManaged public var magneticInfo: String?

}
